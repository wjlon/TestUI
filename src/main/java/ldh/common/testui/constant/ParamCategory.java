package ldh.common.testui.constant;

import ldh.common.testui.vo.*;

/**
 * Created by ldh on 2018/3/17.
 */
public enum ParamCategory {
    Constant("常量", null, ""),
    Spring("spring配置", new SpringParam(), null),
    Database("数据库配置", new DatabaseParam("", "", "", "com.mysql.jdbc.Driver", ""), null),
    Method_Package("测试类路径", null, "**.**.**(包路径）"),
//    Sql("SQL", new SqlParam()),
    Other_jar("增加外部类", null, "e://other//demo"),
    Increment("递增变量", new IncrementParam(), null),
    ;

    private String desc;
    private ParamInterface paramInterface;
    private String profitString;

    private ParamCategory(String desc, ParamInterface paramInterface, String profitString) {
        this.desc = desc;
        this.paramInterface = paramInterface;
        this.profitString = profitString;
    }

    public String getDesc() {
        return desc;
    }

    public ParamInterface getParamInterface() {
        return paramInterface;
    }

    public String getProfitString() {
        return profitString;
    }
}
