package ldh.common.testui.dao;

import ldh.common.testui.model.GetResultValue;
import ldh.common.testui.util.DbUtils;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by ldh on 2019/12/3.
 */
public class GetResultValueDao {

    public static void save(GetResultValue getResultValue) {
        if (getResultValue.getId() != null && getResultValue.getId() != 0) {
            update(getResultValue);
        } else {
            insert(getResultValue);
        }
    }

    public static void insert(GetResultValue getResultValue) {
        ResultSetHandler<Long> h = new ScalarHandler();
        String insertSql = "insert into get_result_value(tree_node_id, name, value, type) values(?, ?, ?, ?)";
        try {
            Long id = DbUtils.getQueryRunner().insert(insertSql, h, getResultValue.getTreeNodeId(), getResultValue.getName(), getResultValue.getValue(), getResultValue.getType());
            getResultValue.setId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<GetResultValue> getAll() throws SQLException {
        String sql = "select id, tree_node_id as treeNodeId, name, value, type from get_result_value";
        ResultSetHandler<List<GetResultValue>> beanHandler = new BeanListHandler<>(GetResultValue.class);
        return DbUtils.getQueryRunner().query(sql, beanHandler);
    }

    public static GetResultValue getById(Integer id) throws SQLException {
        String sql = "select id, tree_node_id as treeNodeId, name, value, type from get_result_value where id = ?";
        ResultSetHandler<GetResultValue> beanHandler = new BeanHandler<GetResultValue>(GetResultValue.class);
        return DbUtils.getQueryRunner().query(sql, beanHandler, id);
    }

    public static List<GetResultValue> getByTreeNodeId(int treeNodeId) throws SQLException {
        String sql = "select id, tree_node_id as treeNodeId, name, value, type from get_result_value where tree_node_id = ?";
        ResultSetHandler<List<GetResultValue>> beanHandler = new BeanListHandler<>(GetResultValue.class);
        return DbUtils.getQueryRunner().query(sql, beanHandler, treeNodeId);
    }

    public static void delete(GetResultValue getResultValue) throws SQLException {
        DbUtils.getQueryRunner().update("delete from get_result_value where id = ?", getResultValue.getId());
    }

    public static void deleteByTreeNodeId(int treeNodeId) {
        try {
            DbUtils.getQueryRunner().update("delete from get_result_value where tree_node_id = ?", treeNodeId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void update(GetResultValue getResultValue) {
        try {
            DbUtils.getQueryRunner().update("update get_result_value set name = ?, value = ?, tree_node_id=?, type=? where id = ?",
                    getResultValue.getName(), getResultValue.getValue(), getResultValue.getTreeNodeId(), getResultValue.getType(), getResultValue.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
